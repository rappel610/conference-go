import json
import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    resp = requests.get(url, params=params, headers=headers)
    return resp.json()["photos"][0]["url"]


def get_weather_data(city, state):
#Get the lat and ln of the city
    params = {
        "q": f"{city},{state}, US",
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = response.json()

    latitude = content[0]["lat"]
    longitude = content[0]["lon"]
#Get the weather
    params = {
        "lat": latitude,
        "lon": longitude,
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    json_resp = response.json()

    temp = json_resp["main"]["temp"]
    desc = json_resp["weather"][0]["description"]

    return {"temp": temp, "description": desc}
