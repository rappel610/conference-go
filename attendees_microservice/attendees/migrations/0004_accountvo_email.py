# Generated by Django 4.0.3 on 2023-02-15 23:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0003_remove_accountvo_is_active'),
    ]

    operations = [
        migrations.AddField(
            model_name='accountvo',
            name='email',
            field=models.EmailField(default='', max_length=254, unique=True),
            preserve_default=False,
        ),
    ]
