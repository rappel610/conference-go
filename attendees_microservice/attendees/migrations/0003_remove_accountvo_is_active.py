# Generated by Django 4.0.3 on 2023-02-15 23:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('attendees', '0002_accountvo'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='accountvo',
            name='is_active',
        ),
    ]
